<?php

declare (strict_types = 1);
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Product{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
   public $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="5")
     */
   public $name;
    /**
     * @ORM\Column(type="float") //verif coté sql
     * @Assert\Type(type="float") //verif coté applicatif
     */
   public $price;

}
?>


