<?php

declare (strict_types = 1);

namespace App\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

use App\Entity\Product;

class GetProduct{
    public function __invoke(Request $request,EntityManagerInterface $em){
        $repository = $em->getrepository(Product::class);
        $productId=$request->get('id');
       
        dump($productId);
        $product = $repository->find($productId);
        return new JsonResponse($product);
    }
}
?>


