<?php

declare (strict_types=1);


class Logger{
	public function log(){
		echo 'Log un truc';
	}
}

class Monolog{
	public function warning(){
		echo 'Log un truc';
	}
}

class Mailer{
	public $logger;
	public function _construct(Monolog $logger){
		$this->logger =$logger;
	}

	public function sendMail(){
	}
}
class Controller{
	public $mailer;
	public $logger;

	public function _construct(Mailer $mailer, Logger $logger){
		$this->mailer =$mailer;
		$this->logger =$logger;

	}
	public function index(){
		$this->mailer->sendMail();
		$this->logger->log();

	}
}
?>


