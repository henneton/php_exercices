<?php

// Le nom de la classe doit tjrs avoir le meme nom que le fichier
class Index{

	public function displayName():string{
		return 'Claire';
	}

	public function displayNameWithParameters(string $name):string{
		$name= "Hello $name";
		return $name;
	}

	public function displayNameWithObjectParameters(UserInterface $user):string{
		$user->setName($user->getName()." world");
		return $user->getName();
	}
}

interface UserInterface{
	public function getName():string;
	public function setName(string $name):void;
	public function getUserName():string;
}

Abstract class User implements UserInterface{ // on peut pas faire un new sur une classe abstraite
	protected $name;
	public static $count=0;
	public function getName():string{
		static::$count+=1;
		return $this->name;
	}
	public function setName(string $name):void{
		$this->name = $name;
	}

	abstract public function getUserName():string;
	
}

class Admin extends User{ // les attributs de user doivent etre public ou protected pour que Admin en herite egalement
	public function getUserName():string{
		return "Admin {$this->name}";
	}
}

class AdvancedUser extends User{ 
	public function getUserName():string{
		return $this->name;
	}
}


######################################### MAIN ############################################################################

$index = new Index();
$user = new AdvancedUser();
$user->setName('Henri');

$admin = new Admin();
$admin->setName('Gautier');
// echo $index->displayName().PHP_EOL;  // PHP_EOL retour à la ligne

// $index=null; //destruction de la valeur
// unset($index); //destruction de la variable
//$name= 'Laury';
//echo $index->displayNameWithParameters($name).PHP_EOL;
echo $index->displayNameWithObjectParameters($user).PHP_EOL;

var_dump($user); //affiche le contenu d'une variable mais aussi son type
var_dump($user::$count); 
var_dump($admin instanceof UserInterface); 

echo $index->displayNameWithObjectParameters($admin).PHP_EOL;

//exit(128);

################################# LES PRINCIPES #######################


// Principes solides : 
// - Responsabilité unique (single responsibility principle)
// 	une classe, une fonction ou une méthode doit avoir une et une seule responsabilité
// - Ouvert/fermé (open/closed principle)
// 	une classe doit être ouverte à l'extension, mais fermée à la modification
// - Substitution de Liskov (Liskov substitution principle)
// 	une instance de type T doit pouvoir être remplacée par une instance de type G, tel que G sous-type de T, sans que cela ne modifie la cohérence du programme
// - Ségrégation des interfaces (interface segregation principle)
// 	préférer plusieurs interfaces spécifiques pour chaque client plutôt qu'une seule interface générale
// - Inversion des dépendances (dependency inversion principle)
// 	il faut dépendre des abstractions, pas des implémentations

################################# CLASSES & OBJETS #######################

// final devant une class: bloquer l'heritage d'une classe
// static : partager à travers toutes les intances (sur attr et méthodes)
// class abstraite : interet ? mettre des fonctions décalrées dans les enfants
// Une classe est tjrs plublic

// interface : interet ? tous le monde a la meme signature

?>


